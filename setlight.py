#!/usr/bin/python
import requests
from time import sleep
import json
import sys
from light import Light
from os.path import dirname, join


numlights = 10
profile = None

def usage():
    print "./setlight.py (all|light#) (full|[0-255]) [relax|reading|concentrate|energize]"
    sys.exit(0)

if(len(sys.argv) < 2):
    usage()

light = sys.argv[1]

bri = int(sys.argv[2])

if(len(sys.argv) > 3):
    profile = sys.argv[3]


if light.strip() == 'all':
    lights = [Light(lightnum=x) for x in range(1, numlights+1)]
else:
    lights = [Light(lightnum=light)]

for light in lights:
    if(profile):
        light.on()
        getattr(light, profile)()
    else:
        if(bri == 0):
            light.off()
        else:
            light.brightness(bri)
