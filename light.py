#!/usr/bin/python
import requests
from time import sleep
import json
import ConfigParser
import os
from os.path import dirname, join

CONFIGFILE = join(dirname(__file__), 'hue.cfg')
API_URL = 'https://client-eastwood-dot-hue-prod-us.appspot.com/api/nupnp'

class Light:
    def __init__(self, ip=None, secret=None, lightnum=None, debug=False):
        self.secret = None
        self.debug = debug
        self.ip = self.getIP()
        #If a config is available, default to it
        if os.path.isfile(CONFIGFILE):
            config = ConfigParser.RawConfigParser()
            config.read(CONFIGFILE)
            if config.has_option('hue', 'secret'):
                self.secret = config.get('hue', 'secret')
            if config.has_option('hue', 'light'):
                self.lightnum = config.get('hue', 'light')
            else:
                self.lightnum = 1
        else:
            print "No config found"
        #Fill in if parameter was set
        if(secret): self.secret = secret
        if(lightnum): self.lightnum = lightnum
        if(not self.secret):
            self.register()

    def getIP(self):
        url = API_URL
        r = requests.get(url)
        data = json.loads(r.content)
        if(len(data) > 0 and data[0].has_key('internalipaddress')):
            return data[0]['internalipaddress']
        else:
            import ssdp
            from urlparse import urlparse
            url = ssdp.client()
            if url:
                return urlparse(url).hostname
            else:
                print "Can't find bridge"
                return None
        return None

    def register(self):
        secret = None
        while not secret:
            body = json.dumps({'username': 'bettseLight', 'devicetype': 'python'})
            url = 'http://%s/api/' % (self.ip)
            r = requests.post(url, data=body)
            data = json.loads(r.content)[0]
            if(data.has_key('success')):
                secret = data['success']['username']
                print "Key is %s" % secret
            if(data.has_key('error')):
                print "Please push the button on the Phlips Hue Hub"
                sleep(0.5)
        self.secret = secret
        configfile = open(CONFIGFILE, 'w+')
        config = ConfigParser.RawConfigParser()
        if not config.has_section("hue"):
            config.add_section("hue")
        config.set('hue', 'secret', self.secret)
        config.set('hue', 'light', self.lightnum)
        config.set('hue', 'ip', self.ip)
        config.write(configfile)

    def setstate(self, body):
        if(type(body) != str):
            body = json.dumps(body)
        if(self.debug):
            print "Send %s to light %s" % (body, self.lightnum)
        url = 'http://%s/api/%s/lights/%s/state' % (self.ip, self.secret, self.lightnum)
        r = requests.put(url, data=body)

    def brightness(self, i):
        if(i == 'full'):
            i = 254
        if(int(i) > 254):
            i = 254
        bri = json.dumps({'bri': int(i), 'on': True})
        self.setstate(bri)

    def on(self):
        body = json.dumps({'on': True})
        self.setstate(body)

    def off(self):
        body = json.dumps({'on': False})
        self.setstate(body)

    def number(self):
        return self.lightnum

    def getstate(self):
        url = 'http://%s/api/%s/lights/%s/' % (self.ip, self.secret, self.lightnum)
        r = requests.get(url)
        return json.loads(r.content)['state']

    def colortemp(self, i):
        body = json.dumps({'colormode': 'ct', 'ct': i})
        self.setstate(body)

    def concentrate(self):
        body = json.dumps({u'on': True, u'hue': 13122, u'colormode': u'ct', u'bri': 219, u'sat': 211, u'ct': 233})
        self.setstate(body)

    def energize(self):
        body = json.dumps({u'on': True, u'hue': 13122, u'colormode': u'ct', u'bri': 203, u'sat': 211, u'ct': 156})
        self.setstate(body)

    def reading(self):
        body = json.dumps({u'on': True, u'hue': 13122, u'colormode': u'ct', u'bri': 240, u'sat': 211, u'ct': 346})
        self.setstate(body)

    def relax(self):
        body = json.dumps({u'on': True, u'hue': 13122, u'colormode': u'ct', u'bri': 144, u'sat': 211, u'ct': 467})
        self.setstate(body)

    def red(self):
        self.setstate({"on": True, "hue": 836, "colormode": "xy", "xy": [0.6475, 0.3316]})

    def blue(self):
        self.setstate({"on": True, "hue": 47103, "colormode": "xy", "xy": [0.167, 0.04]})

    def green(self):
        self.setstate({"on": True, "hue": 47103, "colormode": "xy", "xy": [0.3991, 0.4982]})

    def white(self):
        self.setstate({"on": True, "hue": 47103, "colormode": "xy", "xy": [0.3355, 0.3595]})

