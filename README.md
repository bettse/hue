
# Requirements:
 * Requests library (http://docs.python-requests.org/en/latest/user/install/#install)

# First Use
 1. Run ./setlight.py all 100
 1. It prompt you to push the button on the Hue controller, do it.
 1. You can now run scripts like setlight.py or pulse.py, or write your own.


# Existing scripts
 * fadeon.py fades lights on over 600 seconds
 * pulse.py pulses lights on and off with 1 second sleep between
 * setlight.py takes light number and brightness parameters and sets light(s) on and to that brightness

# Misc
All the info on the API I got from http://www.nerdblog.com/2012/10/a-day-with-philips-hue.html

